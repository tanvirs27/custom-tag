<%@ page import="net.therap.customtags.Item" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%--
  User: shahriar
  Date: 2/26/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://therap.net" prefix="m" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Custom Tag</title>
</head>
<body>

<%
    List<String> firstRow = new ArrayList<>();
    firstRow.add("a");
    firstRow.add("b");
    firstRow.add("c");

    List<String> secondRow = new ArrayList<>();
    secondRow.add("d");
    secondRow.add("e");
    secondRow.add("f");

    List<List<String>> list = new ArrayList<>();
    list.add(firstRow);
    list.add(secondRow);

    Item firstItem = new Item();
    firstItem.setName("bread");
    firstItem.setQuantity(2);

    Item secondItem = new Item();
    secondItem.setName("jelly");
    secondItem.setQuantity(0.2);

    List<Item> genericList = new ArrayList<>();
    genericList.add(firstItem);
    genericList.add(secondItem);
%>

<c:set var="list" value="<%=list%>"/>
<c:set var="genericList" value="<%=genericList%>"/>

<m:table genericList="${genericList}" style="width:20%; border-style:solid; border-width:5px">

    <m:column header="Serial" style="border-style:solid"> auto </m:column>
    <m:column header="Item Name" type="name">
    </m:column>
    <m:column header="Quantity" type="quantity">
    </m:column>

</m:table>

</body>
</html>