package net.therap.customtags;

/**
 * @author shahriar
 * @since 2/26/18
 */

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class TableTag extends BodyTagSupport {

    private List<List<String>> list;
    private List<Column> columns;
    private List genericList;
    private String className;
    private String style;

    @Override
    public int doStartTag() throws JspException {
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() throws JspException {

        JspWriter out = pageContext.getOut();

        if (className != null) {
            className = "class= '" + className + "'";
        } else {
            className = "";
        }

        if (style != null) {
            style = "style= '" + style + "'";
        } else {
            style = "";
        }

        try {
            out.println("<table " + className + " " + style + ">");
            out.println("<thead>");
            out.println("<tr>");

            for (Column column : columns) {
                out.println("<th>" + column.getHeader() + "</th>");
            }

            out.println("</tr>");
            out.println("</thead>");

            out.println("<tbody>");

            int autoIncrementValue = 1;

            if (genericList != null) {
                makeTableForGenericList(out, autoIncrementValue);
            } else if (list != null) {
                makeTableForList(out, autoIncrementValue);
            }

            out.println("</tbody>");
            out.println("</table>");

            columns.clear();
        } catch (IOException e) {
            System.out.println(e.toString());
        }

        return EVAL_BODY_INCLUDE;
    }

    public List<List<String>> getList() {
        return list;
    }

    public void setList(List<List<String>> list) {
        this.list = list;
    }

    public void addColumn(Column column) {
        if (columns == null) {
            columns = new ArrayList<>();
        }
        columns.add(column);
    }

    public List getGenericList() {
        return genericList;
    }

    public void setGenericList(List genericList) {
        this.genericList = genericList;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    private String getColumnValue(String type, Object object) {
        for (Method method : object.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (type.length() + 3))) {
                if (method.getName().toLowerCase().endsWith(type.toLowerCase())) {
                    try {
                        return method.invoke(object).toString();
                    } catch (IllegalAccessException e) {
                        System.err.println("Could not determine method: " + method.getName());
                    } catch (InvocationTargetException e) {
                        System.err.println(e.toString());
                    }
                }
            }
        }
        return null;
    }

    private void makeTableForGenericList(JspWriter out, int autoIncrementValue) throws IOException {

        for (Object row : genericList) {
            out.println("<tr>");

            for (Column column : columns) {

                String columnStyle = column.getStyle();

                if (columnStyle != null) {
                    columnStyle = "style= '" + columnStyle + "'";
                } else {
                    columnStyle = "";
                }

                out.println("<td " + columnStyle + ">");

                String columnType = column.getType();
                if (columnType != null) {
                    out.println(getColumnValue(columnType, row));
                } else if (column.isAuto()) {
                    out.println(String.valueOf(autoIncrementValue++));
                }
                out.println("</td>");
            }

            out.println("</tr>");
        }
    }

    private void makeTableForList(JspWriter out, int autoIncrementValue) throws IOException {
        for (List<String> row : list) {
            out.println("<tr>");

            for (int index = 0; index < columns.size(); index++) {
                String columnStyle = columns.get(index).getStyle();

                if (columnStyle != null) {
                    columnStyle = "style= '" + columnStyle + "'";
                } else {
                    columnStyle = "";
                }

                out.println("<td " + columnStyle + ">");

                if (columns.get(index).isAuto()) {
                    out.println(String.valueOf(autoIncrementValue++));
                } else if (index < row.size()) {
                    out.println(row.get(index));
                }
                out.println("</td>");
            }
            out.println("</tr>");
        }
    }
}
