package net.therap.customtags;

/**
 * @author shahriar
 * @since 2/26/18
 */

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class ColumnTag extends BodyTagSupport {

    private static final String AUTO_FLAG = "auto";

    private String header;
    private String type;
    private String style;
    private Column column;
    private TableTag parent;

    @Override
    public int doStartTag() throws JspException {

        parent = (TableTag) findAncestorWithClass(this, TableTag.class);

        if (parent == null) {
            throw new JspTagException("column is not inside table tag");
        } else {
            column = new Column();
            column.setHeader(header);

            if (type != null) {
                column.setType(type);
            }

            if (style != null) {
                column.setStyle(style);
            }
        }

        return EVAL_BODY_BUFFERED;
    }

    @Override
    public int doAfterBody() throws JspException {

        if (parent == null) {
            return SKIP_BODY;
        }

        BodyContent bodyContent = getBodyContent();

        if (bodyContent != null) {
            column.setAuto(bodyContent.getString().contains(AUTO_FLAG));
        }
        parent.addColumn(column);

        return SKIP_BODY;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
